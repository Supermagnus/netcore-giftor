﻿using System;
using System.Linq;
using System.Threading.Tasks;

namespace Giftor.WebUI.Controllers.Models
{
    public class Prize
    {
        public DateTime WinDate { get; set; }
        public int Sum { get; set; }
    }
}
