﻿using System.Collections.Generic;

namespace Giftor.WebUI.Controllers.Models
{
    public class SpinningWheelViewModel
    {
        public IList<Prize> Prizes { get; set; }
        public bool Enabled { get; set; }
        public int DaysUntilNextSpin { get; set; }
    }
}