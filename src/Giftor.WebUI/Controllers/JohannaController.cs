﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Giftor.WebUI.Controllers.Models;
using Giftor.WebUI.Controllers.Services;
using Microsoft.AspNetCore.Mvc;

namespace Giftor.WebUI.Controllers
{
    public class JohannaController : Controller
    {
        private readonly IPrizeService _prizeService;

        public JohannaController()
        {
            _prizeService = new FileSystemPrizeService();
        }
        public IActionResult Index()
        {
            var model = new SpinningWheelViewModel();
            model.Prizes = _prizeService.GetPrizes("johanna");
            if (model.Prizes.Any(x => x.WinDate.Month == DateTime.Now.Month))
            {
                var nextMonth = new DateTime(DateTime.Now.Year,DateTime.Now.Month,1);

                
                model.Enabled = false;
                model.DaysUntilNextSpin = (int)(nextMonth - DateTime.Now).TotalDays;
            }
            return View("SpinningWheel", model);
        }


        [HttpPost]
        public void RegisterWinning(string prize)
        {
            var intPrize = Regex.Replace(prize, "[^0-9]+", string.Empty);
            _prizeService.RegisterPrize("johanna", int.Parse(intPrize));   
        }
    }


}