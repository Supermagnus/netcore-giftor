﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Giftor.WebUI.Controllers.Models;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using Newtonsoft.Json;

namespace Giftor.WebUI.Controllers.Services
{
    public interface IPrizeService
    {
        IList<Prize> GetPrizes(string name);
        void RegisterPrize(string name, int sum);
    }

    public class FileSystemPrizeService : IPrizeService
    {

        public FileSystemPrizeService()
        {
            if (!Directory.Exists("Data"))
                Directory.CreateDirectory("Data");
;        }
        public IList<Prize> GetPrizes(string name)
        {
            var path = GetPath(name);
            if (!File.Exists(path))
                return new List<Prize>();
            return JsonConvert.DeserializeObject<List<Prize>>(File.ReadAllText(path));
        }

        public void RegisterPrize(string name, int sum)
        {
            var currentPrizes = GetPrizes(name);
            currentPrizes.Add(new Prize()
            {
                Sum = sum,
                WinDate = DateTime.Now
            });
            var content = JsonConvert.SerializeObject(currentPrizes);
            File.WriteAllText(GetPath(name), content, Encoding.UTF8);
        }

        private string GetPath(string name)
        {
            return $"data/{name}.json";
        }
    }
}
